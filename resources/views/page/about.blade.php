@extends('template.master')
@section('content')
@foreach($abouts as $about)
<div class="not-full m-auto">
              <img class="full-image slider" src="{{ url('img/about') }}/{{$about->gbr}}">
</div>
<div class="not-full m-auto news contents animated fadeInUp clearfix">
      <div class="title mt-3">Profil Perusahaan</div>
      <hr class="">
      <div class="text-content">
        <p>
          {!!html_entity_decode($about->isi)!!}
        </p>
    </div>
      <div class="title mt-3">Visi & Misi</div>
    <hr>
          <div class="text-content">
        <p>
          {!!html_entity_decode($about->visi_misi)!!}
        </p>
    </div>        
</div>
@endforeach
@stop