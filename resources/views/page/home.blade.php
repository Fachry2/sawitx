@extends('template.master')
@section('content')
<div id="myCarousel" class="not-full m-auto carousel slide slider" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
          @foreach( $images as $image )
          <li data-target="#myCarousel" data-slide-to="{{ $loop->index }}" class="{{ $loop->first ? 'active' : '' }}"></li>
          @endforeach
      </ol>
      <div class="carousel-inner" role="listbox">
         @foreach( $images as $image )
          <div class="item {{ $loop->first ? ' active' : '' }}" >
            <img src="{{ asset('uploads') }}/{{ $image->filename }}" alt="{{ $image->filename }}">
          </div>
         @endforeach
      </div>
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>

    <div class="not-full m-auto p-2 bg-green text-white weather">
      <div class="row">
        <div class="col-6 mr-auto animated fadeInUp clearfix">
          <div class="row">
            <div class="col-lg-3 col-6">
              <div class="derajat">
                  <span id="temperature"></span><span class="x">°</span>
                  <br>
                  <span id="city"></span><span id="country"></span>
              </div>
            </div>
            <div class="col-lg-9 col-6">
              <div class="text-left">
                <img id="icon" src=""/> <br>
                <span id="weather"></span>
                <span id="description"></span>
              </div>
            </div>
        </div>
            
        </div>
        <div class="col-6 mr-auto animated fadeInUp clearfix">
              @foreach ($prices as $price)
          <div class="sub-title text-white">Harga Sawit hari ini</div >
          <span>Rp.{{number_format($price->harga,0)}}/{{$price->satuan}}</span>
              @endforeach

        </div>
      </div>
    </div>

    <div class="not-full m-auto pt-3 news contents animated fadeInUp clearfix">
      <h3>News</h3>
      <div class="row">
        @foreach ($news as $new)
        <div class="col-lg-4 col-6 mb-5 p-2">
          <div class="news2 doc-item">
            <img class="full-image doc-img animate" src="{{ asset('img/news')}}/{{$new->gbr}}" alt="" />
            <div class="caption p-2 ellipses">
              <span class="sub-title text-white">{{$new->judul}}</span>
              {!!html_entity_decode($new->isi)!!}
            </div>
        </div>
        
        <a href="{{url('/read_news',$new->id)}}" class="text-center">
          <div class="bg-readmore bg-green p-2">
          <span class="sub-title text-white">Read more</span>
          </div>
        </a>
        </div>
        @endforeach
      </div>
    </div>


    <div class="not-full m-auto pt-3 text-center contents animated fadeInUp clearfix">
      <h3>Our Location</h3>
      <img class="mt-5" src="{{ asset('img/location')}}/{{$location->gbr}}">
    </div>

@stop