@extends('template.master')
@section('content')
<div class="not-full m-auto header-img">
  <img class="full-image" src="{{ asset('img/1.jpg')}}">
  <div class="overlay">
    <h1 class="font-light caption p-2">Videos</h1>
  </div>
</div>
 <div class="not-full m-auto pt-3 news contents animated fadeInUp clearfix">
      <div class="row">
        @foreach ($images as $img)
        @if($img->type=="mp4")
        <div class="col-md-6 p-2">
          <div class="video">
          <video controls> <source src="{{ asset('uploads') }}/{{$img->filename}}"/>
        </video>
      </div>
      </div>
      @endif
  @endforeach
      </div>
    </div>


@stop