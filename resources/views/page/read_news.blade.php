@extends('template.master')
@section('content')
<div id="myCarousel" class="not-full m-auto carousel slide slider" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
          @foreach( $images as $image )
          <li data-target="#myCarousel" data-slide-to="{{ $loop->index }}" class="{{ $loop->first ? 'active' : '' }}"></li>
          @endforeach
      </ol>
      <div class="carousel-inner" role="listbox">
         @foreach( $images as $image )
          <div class="item {{ $loop->first ? ' active' : '' }}" >
            <img src="{{ asset('img/news') }}/{{ $image->gbr }}" alt="{{ $image->filename }}">
          </div>
         @endforeach
      </div>
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>

    <div class="not-full m-auto pt-3 contents animated fadeInUp clearfix">
      <h3>{{$new->judul}}</h3>
      <div class="text-justify">
        <p>{!!html_entity_decode(nl2br($new->isi))!!}
      </p>
    </div>
    </div>
@stop