@extends('template.master')
@section('content')
<div class="not-full m-auto header-img">
  <img class="full-image" src="{{ asset('img/1.jpg')}}">
  <div class="overlay">
    <h1 class="font-light caption p-2">Images</h1>
  </div>
</div>
 <div class="not-full m-auto pt-3 news contents animated fadeInUp clearfix">
      <div class="row">
        @foreach ($images as $img)
      @if($img->type=="jpg")
      <div class="col-md-3 col-sm-4 col-6 gal-item">
        <div class="box">
           <a href="#" data-toggle="modal" data-target="#{{$img->id}}">
            <img src="{{ asset('uploads') }}/{{$img->filename}}">
          </a>      
          <div class="modal fade" id="{{$img->id}}" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <div class="modal-body">
                  <img src="{{ asset('uploads') }}/{{$img->filename}}">
                </div>
                  <div class="col-md-12 description">
                    <h4>This is the third one on my Gallery</h4>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>

          @endif
      @endforeach
      </div>     
    </div>
@stop