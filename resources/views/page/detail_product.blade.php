@extends('template.master')
@section('content')
 <section id="section_demo">
            <div class="container">
                <!-- Demo 1 --><br><br><br><br>
               <pre><code>https://www.quandl.com/api/v3/datasets/ODA/PPOIL_USD/data.json?api_key=DPoDtvPjCEBVZLXyrSXQ</code></pre>

                <div class="row mt-7 ">

                    <!-- Result 1 -->
                    <article class="col-lg-12 col-md-12">
                        <div class="row">

                            <div id="slideshow_1_thumbs_1" class="col-lg-2 col-md-2">
                                <ul class="slideshow1_thumbs desoslide-thumbs-vertical list-inline text-center">
                                    <li>
                                        <a href="{{ asset('/img/demos/demo1/bick_buck_bunny.jpg')}}">
                                            <img src="{{ asset('/img/demos/demo1/bick_buck_bunny_thumb.jpg')}}"
                                                 alt="Bick Buck Bunny"
                                                 data-desoslide-caption-title="Bick Buck Bunny">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ asset('/img/demos/demo1/rinky.jpg')}}">
                                            <img src="{{ asset('/img/demos/demo1/rinky_thumb.jpg')}}"
                                                 alt="Rinky"
                                                 data-desoslide-caption-title="Rinky">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ asset('/img/demos/demo1/its_a_trap.jpg')}}">
                                            <img src="{{ asset('/img/demos/demo1/its_a_trap_thumb.jpg')}}"
                                                 alt="It's a trap!"
                                                 data-desoslide-caption-title="It's a trap!">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ asset('/img/demos/demo1/evil_frank.jpg')}}">
                                            <img src="{{ asset('/img/demos/demo1/evil_frank_thumb.jpg')}}"
                                                 alt="Evil Frank"
                                                 data-desoslide-caption-title="Evil Frank">
                                        </a>
                                    </li>
                                </ul>
                            </div>

                            <div id="slideshow1" class="col-lg-8 col-md-8"></div>
                        </div>
                    </article>
                </div>
                <hr>
             
               <hr>
            <form action="{{url('product/upload')}}" method="post" enctype="multipart/form-data" class="dropzone">
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

  <div class="fallback">
    <input name="file" type="file" multiple />
  </div>
  <button type="submit">submit</button>
</form>

@stop
<!-- CSRF Token -->
<script>
    window.csrfToken = '<?php echo csrf_token(); ?>';
    window.dropzonerDeletePath = '<?php echo route('dropzoner.delete') ?>';
</script>