@extends('template.master')
@section('content')
<div class="not-full m-auto header-img">
  <img class="full-image" src="img/1.jpg">
  <div class="overlay">
    <h1 class="font-light caption p-2">Contact</h1>
  </div>
</div>
 <div class="not-full m-auto pt-3 news contents animated fadeInUp clearfix">
      <div class="row">
        <div class="col-lg-6">
           <form action="{{url('/send/message')}}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
                 {{csrf_field()}}
          <div class="text-center">
          <span class="title">Message</span>

@if ( Session::has('flash_message') )
 
  <div class="alert {{ Session::get('flash_type') }}">
      <h3>{{ Session::get('flash_message') }}</h3>
  </div>
  
@endif
        </div>
          <hr>
          <div class="row">
            <div class="col-6 pl-0">
              <label>Name:</label>
              <div class="form-group">
                <input type="text" class="form-control" name="nama">
              </div>
            </div>
            <div class="col-6 pr-0">
              <label>Email:</label>
            <div class="form-group">
              <input type="email" class="form-control" name="email">
            </div>
            </div>
          </div>

          <div class="row">
            <div class="col-6 pl-0">
              <label>Subject:</label>
              <div class="form-group">
                <input type="text" class="form-control" name="subject">
              </div>
            </div>
            <div class="col-6 pr-0">
              <label>Phone:</label>
            <div class="form-group">
              <input type="text" class="form-control" name="tel">
            </div>
            </div>
          </div>
          <div class="form-group">
            <label for="comment">Message:</label>
            <textarea class="form-control" rows="6" id="comment" name="isi"></textarea>
          </div>
          <div class="text-center text-white mb-5">
            <button type="submit" class="btn btn-default" >Send Message</button>
          </div>
         </form>
          </div>
      @foreach($contacts as $contact)
          <div class="col-lg-6 text-center">
            <span class="title">{{$contact->judul}}</span>
            <hr>
            <div class="text-content">
              <p>{!!html_entity_decode($contact->isi)!!}</p>
            </div>
          </div>
      @endforeach

      </div>
    </div>
@stop
