@extends('template.master')
@section('content')
<div class="not-full m-auto header-img">
  <img class="full-image" src="{{ asset('img/1.jpg')}}">
  <div class="overlay">
    <h1 class="font-light caption p-2">Galery</h1>
  </div>
</div>
 <div class="not-full m-auto pt-3 news contents animated fadeInUp clearfix">
      <span class="title">Images</span> <span class="float-right"><a href="{{url('/images/all')}}">Show All</a></span>
      <div class="row">
        @foreach ($images as $img)
      @if($img->type=="jpg")
      <div class="col-md-3 col-sm-4 col-6 gal-item ">
        <div class="box">
           <a href="#" data-toggle="modal" data-target="#{{$img->id}}">
            <img src="{{ asset('uploads') }}/{{$img->filename}}">
          </a>      
          <div class="modal fade" id="{{$img->id}}" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <div class="modal-body">
                  <img src="{{ asset('uploads') }}/{{$img->filename}}">
                </div>
                  <div class="col-md-12 description">
                    <h4>This is the third one on my Gallery</h4>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>

          @endif
      @endforeach
      </div>     
    </div>

    <div class="not-full m-auto news contentx animated fadeInUp clearfix">
      <span class="title">Videos</span> <span class="float-right"><a href="{{url('/videos/all')}}">Show All</a></span>
      <div class="row">
        @foreach ($images as $img)
        @if($img->type=="mp4")
        <div class="col-md-6 p-2">
          <div class="video">
          <video controls> <source src="{{ asset('uploads') }}/{{$img->filename}}"/>
        </video>
      </div>
      </div>
      @endif
  @endforeach
      </div>
    </div>
@stop