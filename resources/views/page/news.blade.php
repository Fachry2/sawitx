@extends('template.master')
@section('content')
<div class="not-full m-auto header-img">
  <img class="full-image" src="img/1.jpg">
  <div class="overlay">
    <h1 class="font-light caption p-2">News</h1>
  </div>
</div>
<div class="not-full m-auto pt-3 news contents animated fadeInUp clearfix">
      <div class="row">
        @foreach ($news as $new)
        <div class="col-lg-4 col-md-6 mb-5 p-2">
          <div class="news2">
            <img class="full-image" src="{{asset('img/news')}}/{{$new->gbr}}" alt="" />
            <div class="caption p-2">
              <span class="sub-title text-white">{{$new->judul}}</span>
              <p class="ellipses">{{$new->isi}}</p>
            </div>
        </div>
        
        <a href="{{url('/read_news',$new->id)}}" class="text-center">
          <div class="bg-readmore bg-green p-2">
          <span class="sub-title text-white">Read more</span>
          </div>
        </a>
        </div>
        @endforeach
      </div>
    </div>

    <div class="not-full m-auto text-center">
        <nav aria-label="Page navigation">
  <ul class="pagination">
    <li>
      <a href="#" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
      </a>
    </li>
    <li><a href="#">1</a></li>
    <li><a href="#">2</a></li>
    <li><a href="#">3</a></li>
    <li><a href="#">4</a></li>
    <li><a href="#">5</a></li>
    <li>
      <a href="#" aria-label="Next">
        <span aria-hidden="true">&raquo;</span>
      </a>
    </li>
  </ul>
</nav>
    </div>
@stop