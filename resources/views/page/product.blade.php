@extends('template.master')
@section('content')
<div class="not-full m-auto pt-2 news contentx">
	<div class="">
		<h1 class="font-light">Products</h1>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
	</div>
	<div class="row mt-3 thumbnails-prod">
		@for ($i=0; $i < 8; $i++)
        <div class="col-md-4 p-2">
            <div class="section-box-ten">
                <!---->
                <figure>
                    <h3>Service</h3>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type</p>
                    <a href="#" class="btn btn-read">Read More</a>
                </figure>
                <img src="{{ asset('img/3.jpg') }}">
            </div>
        </div>
        @endfor
	</div>
</div>
@stop
