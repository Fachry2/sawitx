@extends('admin.layout.master')
@section('param')
About
@stop
@section('content')
<div class="content p-2">
   	<a href="{{ url('/admin/edit/about',$abouts[0]->id) }}" class="title"><span class="float-right glyphicon glyphicon-edit pt-2"></span></a>

	<div id="myCarousel" class="carousel slide slider mt-3" data-ride="carousel">
	      <div class="carousel-inner" role="listbox">
	        <div class="item active">
              <img class="full-image slider" src="{{ url('img/about') }}/{{$abouts[0]->gbr}}">
	        </div>
	      </div>
	</div>
	<div class="news contents">
	      <h3>Profil Perusahaan</h3>
	      <hr>
	      <div class="text-content">
{!!html_entity_decode($abouts[0]->isi)!!}
	    </div>
	    <h3>Visi dan Misi</h3>
	    <hr>
{!!html_entity_decode($abouts[0]->visi_misi)!!}
	</div>
</div>
@stop