@extends('admin.layout.master')
@section('param')
Language
@stop
@section('content')
<div class="content p-2 mt-3">
	@foreach ($bahasas as $bhs)
	<div class="col-6 m-auto">
		<div class="row bg-gray mt-3 p-2">
			<div class="col-10 language">
				<img src="{{ url('img/bendera') }}/{{ $bhs->gbr}}">
				<span class="sub-title pl-3">{{$bhs->bahasa}}</span>
				<span class="sub-title pl-2">|</span>
				<span class="sub-title pl-1">{{$bhs->status}}</span>
			</div>
			<div class="col-2">
				<a href="{{ url('/admin/edit/language',$bhs->id) }}" class="title text-success"><span class="glyphicon glyphicon-edit"></span></a>
				<a href="#" class="title text-danger float-right"><span class="glyphicon glyphicon-trash"></span></a>
			</div>
		</div>
	</div>
	@endforeach
	<div class="col-6 m-auto pt-4 text-center">
		<button type="button" class="btn btn-info btn-large text-white" data-toggle="modal" data-target="#ModalAdd">Add Language</button>

        <div id="ModalAdd" class="modal" role="dialog">
          <div class="modal-dialog text-center">
            <div class="modal-content">
         <form action="{{url('/admin/create/language')}}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
             	   {{csrf_field()}}
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Language</h4>
              </div>
              <div class="modal-body">
                <div class="row">
                	<div class="col-lg-4 admin-image">
		        		<div class="text-center mt-3 form-group">
							<img src="" id="imagePreview"/><br>
						    <input type="file" name="gbr" id="imageUpload" class="hide form-control" /> 
							<label for="imageUpload" class="btn btn-large mt-5 orange"><span class="glyphicon glyphicon-open pr-2" aria-hidden="true"></span>Select image</label>
						</div>
					</div>
					<div class="col-lg-8 text-left">
			            <div class="form-group">
			                <label class="mt-1">bahasa</label>
			                <input type="text" class="form-control" name="bahasa" >
			            </div>
					</div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary text-white" >Save</button>
              </div>
         </form>
            </div>
          </div>
        </div>

	</div>
</div>
@stop
