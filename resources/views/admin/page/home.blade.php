@extends('admin.layout.master')
@section('param')
Home
@stop
@section('content')
<div class="content p-2">
        <div class="bg-success leader mt-3">
    	<div class="row title">
    		<div class="col-10 p-3">Price</div>
    	</div>
    </div>
    @foreach ($prices as $price)
    <div class="row mt-3 p-2">
        <div class="sub-title col-2">Rp.{{$price->harga}}</div>
        <div class="sub-title col-1">{{$price->satuan}}</div>
        <div class="sub-title text-right col-2">{{$price->umur}}Tahun</div>
        <div class="sub-title col-1 text-right">
            <button data-toggle="modal" class="title text-success no-button" data-target="#ModalEdit{{$price->id}}"><span class="float-right glyphicon glyphicon-edit"></span></button>
            <div id="ModalEdit{{$price->id}}" class="modal fade text-center" role="dialog">
              <div class="modal-dialog">
                <div class="modal-content">
                    <form action="{{url('/admin/up/price')}}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
                 {{csrf_field()}}
                    <input type="text" class="form-control hide" name="id" value="{{$price->id}}" >
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Price</h4>
                  </div>
                  <span class="sub-title text-left">Language : Indonesia</span>
                  <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-4">
                           <label class="mt-1">Harga</label>
                        <input type="text" class="form-control" name="harga" value="{{$price->harga}}">
                    </div>
                    <div class="form-group col-4">
                        <label class="mt-1">Berat</label>
                        <input type="text" class="form-control" name="satuan" value="{{$price->satuan}}">
                    </div>
                    <div class="form-group col-4">
                        <label class="mt-1">Keterangan</label>
                        <input type="text" class="form-control" name="umur" value="{{$price->umur}}">
                        </div>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary text-white" >Save</button>
                  </div>
                </div>
            </form>
              </div>
            </div>
        </div>
        <div class="sub-title col-1">
        <form action="{{url('admin/delete/price',$price->id)}}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
              <input name="_method" type="hidden" value="DELETE">
                 {{csrf_field()}}
            <button type="submit" class="title text-danger no-button"><span class="glyphicon glyphicon-trash"></span></button>
        </form>
        </div>
    </div>
    @endforeach
    <div class="text-center mt-3">
        <button type="button" class="btn btn-info btn-large text-white" data-toggle="modal" data-target="#ModalAdd">Add</button>
        <div id="ModalAdd" class="modal " role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">

             <form action="{{url('/admin/add/price')}}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
                 {{csrf_field()}}
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Price</h4>
              </div>
              <span class="sub-title text-left">Language : Indonesia</span>
              <div class="modal-body">
                <div class="row">
                    <div class="form-group col-4">
                        <label class="mt-1">Harga</label>
                        <input type="text" class="form-control" name="harga">
                    </div>
                    <div class="form-group col-4">
                        <label class="mt-1">Berat</label>
                        <input type="text" class="form-control" name="satuan">
                    </div>
                    <div class="form-group col-4">
                        <label class="mt-1">Keterangan</label>
                        <input type="text" class="form-control" name="umur">
                    </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary text-white">Save</button>
              </div>
            </form>

            </div>
          </div>
        </div>
    </div>

    <div class="bg-success leader mt-3">
    	<div class="row title">
    		<div class="col-10 p-3">Location</div>
    		<div class="col-2 p-4">
			<a href="{{ url('/admin/edit/location') }}" class="title"><span class="float-right glyphicon glyphicon-edit"></span></a>
    		</div>
    	</div>
    </div>
    <div class="text-center mt-3">
    	<img src="{{ asset('img/location')}}/{{$location->gbr}}" id="imagePreview"/><br>
	</div>

</div>

@stop