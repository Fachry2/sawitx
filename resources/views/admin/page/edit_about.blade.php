@extends('admin.layout.master')
@section('param')
Edit/About
@stop
@section('content')
<div class="content p-2">
   <form action="{{url('/admin/update/about')}}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
                 {{csrf_field()}}
    <input type="text" name="id" value="{{$about->id}}" class="form-control hide">
    <input type="text" name="lang" value="{{$about->lang}}" class="form-control hide">
   
	<div class="text-center mt-3">
		<img src="{{ asset('img/material/no_image.png') }}" id="imagePreview"/><br>
	    <input type="file" name="gbr" id="imageUpload" class="hide"/> 
		<label for="imageUpload" class="btn btn-large mt-5 orange"><span class="glyphicon glyphicon-open pr-2" aria-hidden="true"></span>Select image</label>
	</div>
    <div class="form-group p-2">
        <label class="mt-1">Language :</label>
        @include('admin.page.pil_bhs')
    </div>
    <div class="form-group p-2">
        <label class="mt-1">Profil</label>
        <textarea class="form-control summernote" id="exampleTextarea" name="isi" rows="3">{!!html_entity_decode(nl2br($about->isi))!!}
</textarea>
    </div>
    <div class="form-group p-2">
        <label class="mt-1">Visi & Misi</label>
        <textarea class="form-control summernote" id="exampleTextarea" name="visi_misi" rows="3">{!!html_entity_decode(nl2br($about->visi_misi))!!}
</textarea>
    </div>

	<div class="container col-12 mb-3 p-2">
		<button type="submit" class="btn btn-large bg-orange float-right text-white">Save</button>
	</div>
</form>
</div>
@stop