@extends('admin.layout.master')
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>
    jQuery(document).ready(function(){
          console.log( "ready!" );

        jQuery('.laradrop').laradrop({
        breadCrumbRootText: 'My Root', // optional 
        actionConfirmationText: 'Sure about that?', // optional
        onInsertCallback: function (obj){ // optional 
            // if you need to bind the select button, implement here
                 $('#my_image').attr('src',obj.src);
        }
    });

    });
    function on() {
    $('#myModal').modal('show'); 
    }
    </script>
@section('param')
Add/Media
@stop
@section('content')
<div class="content p-2">
        <div class="row mt-3">
    	<div class="col-8 pl-0 pr-2">
    		<div class="bg-white p-2">
	   <div class="laradrop" laradrop-csrf-token="{{ csrf_token() }}"> </div>
    		</div>
    	</div>
		<div class="col-4 bg-white">
			image
		</div>
    </div>
	<div class="mb-3 mt-3 p-2">
		<button type="button" class="btn btn-large bg-orange float-right text-white">Save</button>
	</div>

</div>
          <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <div class="modal-body">
                  <img src="" id="my_image">
                </div>
                  <div class="col-md-12 description">
                    <h4>This is the third one on my Gallery</h4>
                  </div>
              </div>
            </div>
          </div>
@stop

