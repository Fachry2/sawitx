@extends('admin.layout.master')
@section('param')
News
@stop
@section('content')
<div class="content p-2">
	<div class="container col-12">
	<a href="{{ url('/admin/add/news') }}" class="btn btn-primary btn-large text-white float-right">Add News</a>
</div>
	<div class="row p-5 mt-4">
        @foreach ($news as $new)
		<div class="col-lg-4 col-md-6 mb-5 p-2">
          	<div class="single_portfolio_text news2">
	            <img class="full-image" src="{{asset('img/news')}}/{{$new->gbr}}"/>
	            <div class="caption p-2 ellipses2">
	              <span class="sub-title text-white">{{$new->judul}}</span>
	              {!!html_entity_decode($new->isi)!!}
	            </div>
	            <div class="portfolio_images_overlay text-center">
		            <a href="{{url('/read_news',$new->id)}}" class="btn btn-primary btn-large text-white mt-5">Show</a> <br>
		            <a href="{{ url('/admin/edit/news',$new->id) }}" class="btn btn-primary btn-large mt-2 text-white">Edit</a><br>

		             <form action="{{url('admin/delete/news',$new->id)}}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
              <input name="_method" type="hidden" value="DELETE">
                 {{csrf_field()}}
		            <button type="submit" class="btn btn-primary btn-large mt-2 text-white">Delete</button>
		            </form>
		        </div>
        	</div>
        </div>
		@endforeach
	</div>
</div>
@stop