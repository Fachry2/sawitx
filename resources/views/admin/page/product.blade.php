@extends('admin.layout.master')
@section('param')
Product
@stop
@section('content')
<div class="content p-2">
    <span class="sub-title">Language :</span>
          @include('admin.page.pil_bhs')
	<div class="container">
	<a href="{{ url('/admin/add/product') }}" class="btn btn-primary btn-large text-white float-right">Add Product</a>
</div>
	<div class="row p-5 mt-4">
		@for ($i = 0; $i < 8; $i++)
		<div class="col-lg-3 col-md-4 mb-5 p-3">
			<div class="single_portfolio_text">
				<img src="{{ asset('img/3.jpg') }}">
				<div class="p-2">
	                <div class="title">
	                    Example library
	                </div>
	                <div class="content-news text-justify">
	                    Indonesia mendukung pelaksanaan studi yang komprehensif untuk melihat dampak dari rencana pemberlakuan ASEAN-Kanada FTA. Indonesia mendukung pelaksanaan studi yang komprehensif untuk melihat dampak dari rencana pemberlakuan ASEAN-Kanada FTA.
	                </div>
	            </div>
		        <div class="portfolio_images_overlay text-center">
		            <a href="#" class="btn btn-primary btn-large text-white mt-5">Show</a> <br>
		            <a href="{{ url('/admin/edit/product') }}" class="btn btn-primary btn-large mt-2 text-white">Edit</a><br>
		            <a href="#" class="btn btn-primary btn-large mt-2 text-white">Delete</a>
		        </div>
    </div>
		</div>
		@endfor
	</div>
</div>
@stop