@extends('admin.layout.master')
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>
    jQuery(document).ready(function(){
        jQuery('.laradrop').laradrop({
        breadCrumbRootText: 'Galery', // optional 
        actionConfirmationText: 'Sure about that?', // optional
        onInsertCallback: function (obj){ // optional 
            // if you need to bind the select button, implement here
            console.log(obj);
                 $('#my_image').attr('src',obj.src);
        }
    });
    });
    function on() {
    $('#myModal').modal('show'); 
    }
    function preview() {
     $('.preview_galery').show();
      $('.hiden').hide();
    }
     function hide() {
     $('.preview_galery').hide();
      $('.hiden').show();
    }
    </script>
@section('param')
Galery & SLider
@stop
@section('content')
<div class="content p-2 hiden">
 <div class="laradrop" laradrop-csrf-token="{{ csrf_token() }}"> </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <div class="modal-body">
                  <img src="" id="my_image">
                </div>
                  <div class="col-md-12 description">
                    <h4>This is the third one on my Gallery</h4>
                  </div>
              </div>
            </div>
</div>


<div class="content p-2 preview_galery">
    <div class="bg-success leader mt-3">
    	<div class="row title">
    		<div class="col-10 p-3">Image</div>
            <button class="btn btn-primary float-right " onclick="hide()">hide</button>  
    	</div>
    </div>

    <div class="row p-3">
	    @foreach ($images as $img)
          @if($img->type=="jpg")
      <div class="col-md-3 col-sm-6 co-xs-12 gal-item">
        <div class="box">
           <a href="#" data-toggle="modal" data-target="#{{$img->id}}">
            <img src="{{ asset('uploads') }}/{{$img->filename}}">
          </a>      
          <div class="modal fade" id="{{$img->id}}" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <div class="modal-body">
                  <img src="{{ asset('uploads') }}/{{$img->filename}}">
                </div>
                  <div class="col-md-12 description">
                    <h4>This is the third one on my Gallery</h4>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>

          @endif
      @endforeach
	  </div> 
    <div class="bg-success leader mt-3">
    	<div class="row title">
    		<div class="col-10 p-3">Video</div>
    	</div>
    </div>
    <div class="container mt-3">
    <button type="button" class="btn btn-info btn-large text-white float-right" data-toggle="modal" data-target="#ModalAddVideo">Add Video</button>
        <div id="ModalAddVideo" class="modal fade" role="dialog">
          <div class="modal-dialog text-center">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Video</h4>
              </div>
              <div class="modal-body">
                <div class="row">
                	form input Video
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary text-white" data-dismiss="modal">Save</button>
              </div>
            </div>
          </div>
        </div>
    </div>
	  <div class="row">
	  @foreach ($images as $img)
        
      @if($img->type!=null)
   <div class="text-center mr-auto">
     Video {{$img->type}}Kosong
   </div>
   @elseif($img->type=="mp4")
        <div class="col-md-6 p-2">
          <div class="video">
          <video controls> <source src="{{ asset('uploads') }}/{{$img->filename}}"/>
        </video>
      </div>
      </div>
      @endif
	@endforeach
	</div>
</div>
@stop