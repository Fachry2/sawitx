@extends('admin.layout.master')
@section('param')
Add/Product
@stop
@section('content')
<div class="content p-2">
    <span class="sub-title">Language :</span>
        @include('admin.page.pil_bhs')
    <div class="row mt-3">
    	<div class="col-8 pl-0 pr-2">
    		<div class="bg-white p-2">
	            <div class="form-group">
	                <label class="mt-1">Title</label>
	                <input type="text" class="form-control">
	            </div>
	            <div class="form-group">
	                <label class="mt-1">Content</label>
	                <textarea class="form-control" id="exampleTextarea" rows="3"></textarea>
	            </div>
    		</div>
    	</div>
		<div class="col-4 bg-white">
			image
		</div>
    </div>
	<div class="mb-3 mt-3 p-2">
		<button type="button" class="btn btn-large bg-orange float-right text-white">Save</button>
	</div>
</div>
@stop