@extends('admin.layout.master')
@section('param')
Add/News
@stop
@section('content')
<div class="content p-2">
	 <form action="{{url('/admin/create/news')}}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
                 {{csrf_field()}}
        <div class="row mt-3">
    	<div class="col-8 pl-0 pr-2">
    		<div class="bg-white p-2">
	            <div class="form-group">
	                <label class="mt-1">Title</label>
	                <input type="text" class="form-control" name="judul">
	            </div>
	            <div class="form-group">
	            	 <label class="mt-1">Language :</label>
           			@include('admin.page.pil_bhs')
	            </div>
	            <div class="form-group">
	                <label class="mt-1">Content</label>
	                <textarea class="form-control summernote" id="" rows="3" name="isi"></textarea>
	            </div>
	        </div>
    	</div>
    	<div class="col-4">
			<div class="text-center mt-3 form-group">
							<img src="" id="imagePreview"/><br>
						    <input type="file" name="gbr" id="imageUpload" class="hide form-control" /> 
							<label for="imageUpload" class="btn btn-large mt-5 orange"><span class="glyphicon glyphicon-open pr-2" aria-hidden="true"></span>Select image</label>
			</div>
		</div>
    </div>
	<div class="container col-12 mb-3 mt-3 p-2">
		<button type="submit" class="btn btn-large bg-orange float-right text-white">Save</button>
	</div>
	</form>
</div>
@stop