@extends('admin.layout.master')
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>
    jQuery(document).ready(function(){
        jQuery('.laradrop').laradrop({
        breadCrumbRootText: 'My Root', // optional 
        actionConfirmationText: 'Sure about that?', // optional
        onInsertCallback: function (obj){ // optional 
            // if you need to bind the select button, implement here
            console.log(obj);
                 $('#my_image').attr('src',obj.src);
        }
    });
    });
    function on() {
    $('#myModal').modal('show'); 
    }
    </script>
@section('param')
Edit/Slide
@stop
@section('content')
<div class="content p-2">
 <div class="laradrop" laradrop-csrf-token="{{ csrf_token() }}"> </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <div class="modal-body">
                  <img src="" id="my_image">
                </div>
                  <div class="col-md-12 description">
                    <h4>This is the third one on my Gallery</h4>
                  </div>
              </div>
            </div>
          </div>
@stop