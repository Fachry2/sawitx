@extends('admin.layout.master')
@section('param')
Edit/Contact
@stop
@section('content')
<div class="content p-2">
   <form action="{{url('/admin/update/contact')}}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
                 {{csrf_field()}}
    <input type="text" name="id" value="{{$contact->id}}" class="form-control hide">
	<div class="">
    <span class="sub-title">{{$contact->lang}}:</span>
	</div>
    <div class="form-group">
        <label class="mt-1">Contact</label>
        <textarea class="form-control" id="exampleTextarea " rows="3" name="isi" >{{$contact->isi}}</textarea>
    </div>
	<div class="container col-12 mb-3 mt-3 p-2">
		<button type="submit" class="btn btn-large bg-orange float-right text-white">Save</button>
	</div>
</form>
</div>
@stop