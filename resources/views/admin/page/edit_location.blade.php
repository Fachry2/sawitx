@extends('admin.layout.master')
@section('param')
About
@stop
@section('content')
<div class="content p-2">
	<form action="{{url('/admin/update/location')}}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
                 {{csrf_field()}}
	<div class="text-center mt-3">
		<img src="{{ asset('img/location')}}/{{$location->gbr}}" id="imagePreview"/><br>
	    <input type="file" name="gbr" id="imageUpload" class="hide"/> 
		<label for="imageUpload" class="btn btn-large mt-5 orange"><span class="glyphicon glyphicon-open pr-2" aria-hidden="true"></span>Select image</label>
	</div>

	<div class=" mb-3 p-2">
		<button type="submit" class="btn btn-large bg-orange float-right text-white">Save</button>
	</div>
</form>
</div>
@stop