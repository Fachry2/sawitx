@extends('admin.layout.master')
@section('param')
Contact
@stop
@section('content')
<div class="content p-2">
	<div class="">
       <a href="{{ url('/admin/edit/contact',$contact[0]->id) }}" class="title"><span class="float-right glyphicon glyphicon-edit pt-2"></span></a>
	</div>
    <div class="col-lg-7 mt-3 p-0">
    <span class="title">{{$contact[0]->judul}}</span>
    <p>{{$contact[0]->isi}}</p>
</div>
</div>
@stop