@extends('admin.layout.master')
@section('param')
Messages
@stop
@section('content')
<div class="content p-2">
		@foreach($messages as $message)
		<div class="{{$message->status}} row m-2 p-3">
			<div class="col-1 text-center">
			<img src="{{ asset('img/user.png') }}">
			<span class="sub-title">{{$message->nama}}</span>
		</div>
		<div class="col-10">
			<span class="sub-title">{{$message->email}}|</span>
			<span class="sub-title">{{$message->tel}}</span><br>
			<span class="sub-title">Subject : </span><span class="sub-title">{{$message->subject}}</span>
			<p>{{$message->isi}}</p>
		</div>
		<div class="col-1">
			 <form action="{{url('admin/delete/message',$message->id)}}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
              <input name="_method" type="hidden" value="DELETE">
                 {{csrf_field()}}
			<button type="submit" class="btn btn-default">Delete</button>
		</form>
		 <form action="{{url('admin/read/message',$message->id)}}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
                 {{csrf_field()}}
		<button type="submit" id="{{$message->status}}" class="btn btn-default mt-2"><i class="fa fa-eye" aria-hidden="true"></i></button>
		</form>
		</div>
		</div>
		@endforeach
</div>
@stop
<script type="text/javascript">
	
</script>