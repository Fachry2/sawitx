@extends('admin.layout.master')
@section('param')
Edit/Language
@stop
@section('content')
<div class="content p-2">
	<form action="{{url('/admin/update/language')}}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
             	   {{csrf_field()}}
	<div class="row">
	<input type="text" class="form-control hide" name="id" value="{{ $bahasa->id}}" >
		<div class="col-lg-8 text-left">
            <div class="form-group">
                <label class="mt-1">Bahasa</label>
                <input type="text" class="form-control" name="bahasa" value="{{ $bahasa->bahasa}}">
            </div>
            <span class="sub-title">Status</span><br>
	            <label class="switch">
						<input type="checkbox" name="status" id="toggle-trigger" {{$bahasa->status}}>
  						<span class="toggle round"></span>
				</label>
		</div>
		<div class="col-lg-4 admin-image">
			<div class="text-center mt-3">
				<img src="{{ url('img/bendera') }}/{{ $bahasa->gbr}}" id="imagePreview"/><br>
			    <input type="file" name="gbr" id="imageUpload" class="hide"/> 
				<label for="imageUpload" class="btn btn-large mt-5 orange"><span class="glyphicon glyphicon-open pr-2" aria-hidden="true"></span>Select image</label>
			</div>
		</div>
    </div>
	<div class="container mt-5 mb-3 p-2">
		<button type="submit" class="btn btn-large bg-orange float-right text-white">Save</button>
	</div>
  </form>
</div>
@stop
