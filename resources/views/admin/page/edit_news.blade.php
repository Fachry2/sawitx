@extends('admin.layout.master')
@section('param')
Edit/News
@stop
@section('content')
<div class="content p-2">
	<form action="{{url('/admin/update/news')}}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
                 {{csrf_field()}}
    <input type="text" name="id" value="{{$new->id}}" class="form-control hide">
    <input type="text" name="lang" value="{{$new->lang}}" class="form-control hide">
    <span class="sub-title">{{$new->lang}} :</span>
    <div class="row mt-3">
    	<div class="col-8 pl-0 pr-2">
    		<div class="bg-white p-2">
	            <div class="form-group">
	                <label class="mt-1">Judul</label>
	                <input type="text" class="form-control" name="judul" value="{{$new->judul}}">
	            </div>
	            <div class="form-group">
	                <label class="mt-1">Content</label>
	                <textarea class="form-control summernote" id="exampleTextarea" rows="3" name="isi">{{$new->isi}}</textarea>
	            </div>
    		</div>
    	</div>
		<div class="col-4 bg-white">
			<img src="{{asset('img/news')}}/{{$new->gbr}}">
			<input type="file" name="gbr[]" multiple>
		</div>
    </div>
	<div class=" mb-3 mt-3 p-2">
		<button type="submit" class="btn btn-large bg-orange float-right text-white">Save</button>
	</div>
</form>
</div>
@stop