@extends('admin.layout.auth')

@section('content')
<style type="text/css">
    body {
        background-image:  url("{{asset('img/material/bg.jpg')}}");
    }
</style>
<div class="container mt-5">

    <div class="col-lg-6 m-auto text-white">
        <div class="col-lg-4 col-6 m-auto pt-3">
            <img src="{{asset('img/cdsl.png')}}">
        </div>
        <form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/login') }}">
                        {{ csrf_field() }}
                        <div class="col-10 m-auto pt-5">
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <span class="sub-title text-white">Email</span>
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" autofocus>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <span class="sub-title text-white">Password</span>
                                    <input id="password" type="password" class="form-control" name="password">

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                            </div>
                            <div class="form-group text-center">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="remember"> Remember Me
                                        </label>
                                    </div>
                            </div>

                            <div class="form-group text-center">
                                    <button type="submit" class="btn btn-orange">
                                        Login
                                    </button>
                            </div>                                                        
                        </div>



                    </form>
    </div>

                    

</div>
@endsection
