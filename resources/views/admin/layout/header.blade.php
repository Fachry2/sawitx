 <script type="text/javascript">
         var now={{count($msg)}};

            window.onload =function() {
                var before = localStorage.getItem('message');
                console.log(now,before);
             if(before<now){
                var show= document.getElementById('notif');
               show.style.visibility= "visible";
               }             
            }
            function baca() {
                localStorage.setItem('message',now);
            }
        </script>
<div class="sidebar" data-color="orange" data-image="{{asset('img/sidebar-5.jpg')}}">
    	<div class="sidebar-wrapper">
            <div class="logo">
                <div class="simple-text">
                    Admin
                </div>
            </div>
            <ul class="nav">
                <li class="active">
                    <a href="{{ url('/admin/home') }}">
                        <i class="pe-7s-graph"></i>
                        <p>Home</p>
                    </a>
                </li>
                <li>
                    <a href="{{ url('/admin/news') }}">
                        <i class="pe-7s-user"></i>
                        <p>News</p>
                    </a>
                </li>
                <li>
                    <a href="{{ url('/admin/galery') }}">
                        <i class="pe-7s-news-paper"></i>
                        <p>Galery & slider</p>
                    </a>
                </li>
                <li>
                    <a href="{{ url('/admin/about') }}">
                        <i class="pe-7s-science"></i>
                        <p>About</p>
                    </a>
                </li>
                <li>
                    <a href="{{ url('/admin/contact') }}">
                        <i class="pe-7s-map-marker"></i>
                        <p>Contact</p>
                    </a>
                </li>
                <li>
                    <a href="{{ url('/admin/language') }}">
                        <i class="pe-7s-bell"></i>
                        <p>Language</p>
                    </a>
                </li>
            </ul>
    	</div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header float-right">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    
                </div><a class="navbar-brand orange" href="#">@yield('param')</a>


            </li>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <span>Language : </span>
                        <div class="btn-group">
                          <button type="button" class="btn btn-default dropdown-toggle no-button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                            {{$bhs_admin}} <span class="caret"></span>
                          </button>
                          <ul class="dropdown-menu">
                            @foreach($flags as $flag)
                            <li><a href="{{ url('/admin/cookie/set',$flag->bahasa) }}" >{{$flag->bahasa}}</a></li>
                            @endforeach
                          </ul>
                        </div></li>
                        <li ><a href="{{ url('/admin/message') }}" onclick="baca()">
                                <i class="fa fa-comments fa-icon" aria-hidden="true"></i>
                                <span class="rounded-circle notif text-white glyphicon glyphicon-asterisk" id="notif"></span>
                            </a>
                        </li>
                          <li><a>
                               <form id="logout-form" action="{{ url('/admin/logout') }}" method="POST" >
                                        {{ csrf_field() }}
                                        
                                        <button type="submit" class="no-button" onclick="return confirm('Anda yakin mau keluar ?');">Log out</button>
                                    
                                    </form>
                                    </a>
                        </li>

                    </ul>
                </div>
            </div>
        </nav>
        <script src="{{ asset('js/bootstrap.min.js') }}" type="text/javascript"></script>
       