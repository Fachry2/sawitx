<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="{{ asset('img/favicon.ico')}}">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Admin</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/animate.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/light-bootstrap-dashboard.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/demo.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/bootstrap-grid.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/bootstrap-grid.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/import.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/main.css')}}" rel="stylesheet" type="text/css">

    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="{{ asset('css/pe-icon-7-stroke.css') }}" rel="stylesheet" />
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="{{ asset('vendor/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{ asset('vendor/jasekz/laradrop/css/styles.css')}}" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet" type="text/css">
         <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.min.js" ></script>
        <script src="{{ asset('vendor/jasekz/laradrop/js/enyo.dropzone.js')}}"></script>
        <script src="{{ asset('vendor/jasekz/laradrop/js/laradrop.js')}}"></script>
</head>
<body>
    <div class="wrapper">
        @include('admin.layout.header')

            @yield('content')

        @include('admin.layout.footer')
    </div>
</body>
    <script src="{{ asset('js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/bootstrap-checkbox-radio-switch.js') }}"></script>
    <script src="{{ asset('js/bootstrap-notify.js') }}"></script>
  
    <script src="{{ asset('js/light-bootstrap-dashboard.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>
    
</html>
