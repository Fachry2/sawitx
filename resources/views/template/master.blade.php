<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<title>Sawit</title>

	<link rel="shortcut icon" href="img/3.jpg">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/3.3.7/cyborg/bootstrap.min.css">

	<link href="{{ asset('css/bootstrap.css')}}" rel="stylesheet" type="text/css">
	<link href="{{ asset('css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{ asset('css/bootstrap-theme.css')}}" rel="stylesheet" type="text/css">
	<link href="{{ asset('css/bootstrap-theme.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{ asset('css/bootstrap-grid.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{ asset('css/bootstrap-grid.css')}}" rel="stylesheet" type="text/css">
	<link href="{{ asset('css/import.css')}}" rel="stylesheet" type="text/css">
	<link href="{{ asset('css/main.css')}}" rel="stylesheet" type="text/css">
	<link href="{{ asset('vendor/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
	<link href="/vendor/jasekz/laradrop/css/styles.css" rel="stylesheet" type="text/css">

	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="{{asset('vendor/dropzoner/dropzone/dropzone.min.css')}}">

	<!-- produk slider -->
<!-- 	 <link rel="stylesheet" href="{{ asset('css/mainimg.css')}}" />
 -->    <!--  <link rel="stylesheet" href="{{ asset('css/demoimg.css')}}" /> -->
     <link rel="stylesheet" href="{{ asset('css/magic/magic.min.css')}}"/>
     <link rel="stylesheet" href="{{ asset('css/animate/animate.min.css')}}"/>
     <link rel="stylesheet" href="{{ asset('css/jquery.desoslide.css')}}"/>

     <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-27524593-3', 'sylouuu.github.io');
            ga('send', 'pageview');
     </script>
	<!-- produk slider end-->

</head>

<body>

@include('template.header')

	@yield('content')

@include('template.footer')


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
<script type="text/javascript" src="{{asset('js/weather.js')}}"></script>
<!-- <script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script> -->
<script type="text/javascript" src="{{asset('js/main.js') }}"></script>	

 <script src="{{ asset('js/jquery.js')}}"></script>
 <script src="{{ asset('js/boot/bootstrap.min.js')}}"></script>
 <script src="{{ asset('js/highlight.pack.js')}}"></script>
 <script src="{{ asset('js/jquery.desoslide.js')}}"></script>
 <script src="{{ asset('js/demoimg.js')}}"></script>


<script type="text/javascript">
  $('div.alert').delay(3000).slideUp(300);
</script>
</html>