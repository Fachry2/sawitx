
    <footer class="footer bg-green text-white mt-5 contents">
      <div class="not-full m-auto">
        <span class="sub-title font-light text-white">Find Us</span>
        <div class="row">
          <div class="col-md-5 sosmed">
            <a href="#"><i class="fa fa-facebook fa-2x p-2"></i></a>
            <a href="#"><i class="fa fa-twitter fa-2x p-2"></i></a>
            <a href="#"><i class="fa fa-instagram fa-2x p-2"></i></a>
            <a href="#"><i class="fa fa-youtube-play fa-2x p-2"></i></a>
          </div>
          <div class="col-md-7 text-right copyright">
            Copyright © 2017 Cipta Daya Cipta Sejati Luhur
          </div>
      </div>
    </footer>
