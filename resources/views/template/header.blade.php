      <nav class="not-full header-top m-auto green">
        <div class="navbar-collapse collapse p-0 brand">
          <ul class="nav navbar-nav">
            <li><a href="{{ url('/') }}" class="pl-0">PT. Cipta Daya Sejati Luhur</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right language-home">
                  <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img src="{{ asset('img/bendera')}}/{{$bhs}}"> <span class="caret"></span></a>
              <ul class="dropdown-menu">
                  @foreach($flags as $flag)
                    <li class="text-center"><a href="{{ url('/cookie/set',$flag->bahasa)}}"><img src="{{ asset('img/bendera')}}/{{$flag->gbr}}">
                    </a></li>
                  @endforeach
              </ul>
            </li>
            <li>
                <form class="navbar-form navbar-left search-form">
                  <div class="form-group">
                    <input type="text" class="form-control font-light" placeholder="Search news">
                  <a href="#" class=""><span class="glyphicon glyphicon-search" aria-hidden="true"></span></a>
                  </div>
                </form>
            </li>
          </ul>
        </div>
    </nav>
    <div class="navbar-wrapper">
      <nav class="not-full-2 m-auto navbar-orange rounded-nav">
            <div class="navbar-header">
              <div class="row">
              <div class="col-6 in-rps">
             <a href="{{ url('/') }}" class="navbar-brand">CDSL</a>
           </div>
           <div class="col-6 float-right">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
            </div>
            </div>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
              <ul class="nav navbar-nav navbar-left">
                <li><a href="{{ url('/') }}">Home</a></li>
                <li><a href="{{ url('/news') }}">News</a></li>
               <!--  <li><a href="{{ url('/product') }}">Products</a></li> -->
              </ul>
              <ul class="nav navbar-nav navbar-right">
                <li><a href="{{ url('/gallery') }}">Galery</a></li>
                <li><a href="{{ url('/about') }}">About</a></li>
                <li><a href="{{ url('/contact') }}">Contact</a></li>

                <li class="dropdown in-rps">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Language <span class="caret"></span></a>
                  <ul class="dropdown-menu">
                        @foreach($flags as $flag)
                    <li><a href="{{ url('/cookie/set',$flag->bahasa)}}">{{$flag->bahasa}}</a></li>
                        @endforeach

                  </ul>
                </li>

                <li class="in-rps">
                  <form class="navbar-form navbar-left search-form pt-0 mt-0">
                  <div class="form-group">
                    <div class="row pl-4">
                      <div class="col-6 p-0">
                        <input type="text" class="form-control" placeholder="Search">
                      </div>
                        <a href="#"><span class="glyphicon glyphicon-search text-white p-3 sub-title" aria-hidden="true"></span></a>
                  </div>
                  </div>
                </form>
                </li>
              </ul>
            </div>
        </nav>
        <div class="col-3 text-center m-auto logo-brand">
        <img src="{{asset('img/cdsl.png')}}">
        </div>
  </div>