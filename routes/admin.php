<?php
header('Access-Control-Allow-Origin: *');
header( 'Access-Control-Allow-Headers: Authorization, Content-Type' );
header("Access-Control-Allow-Headers: X-AMZ-META-TOKEN-ID, X-AMZ-META-TOKEN-SECRET");
header('Access-Control-Allow-Headers: Authorization,x-xsrf-token,Content-Type');

/*Route::get('/home', function () {
    $users[] = Auth::user();
    $users[] = Auth::guard()->user();
    $users[] = Auth::guard('admin')->user();

    //dd($users);

    return view('admin.page.home');
})->name('home');*/
  Route::get('/cookie/set/{lang}','AdminHomeController@setCookie');

 
  //home
  Route::get('/home','AdminHomeController@index');
  Route::post('/add/price','AdminHomeController@add_price');
  Route::post('/up/price','AdminHomeController@up_price');
  Route::delete('/delete/price/{id}','AdminHomeController@delete');


  //location
  Route::get('/edit/location','AdminHomeController@editlocation');
  Route::post('/update/location','AdminHomeController@update'); 

  //news
  Route::get('/news','AdminNewsController@news');
  Route::get('/add/news','AdminNewsController@addnews'); 
  Route::get('/edit/news/{id}','AdminNewsController@editnews');  
  Route::post('/create/news','AdminNewsController@create'); 
  Route::post('/update/news','AdminNewsController@update');  
  Route::delete('/delete/news/{id}','AdminNewsController@destroy');  

  //belom terpake
  Route::get('/product','AdminProductController@product');
  Route::get('/edit/slide','AdminHomeController@editslide');
  Route::get('/edit/product','AdminProductController@editproduct'); 
  Route::get('/add/product','AdminProductController@addproduct');

//about
  Route::get('/about','AdminAboutController@about');
  Route::get('/edit/about/{id}','AdminAboutController@edit');  
  Route::post('/update/about','AdminAboutController@update'); 
  Route::post('/test','AdminAboutController@test'); 

//contact
  Route::get('/edit/contact/{id}','AdminContactController@editcontact');
  Route::get('/contact','AdminContactController@contact');
  Route::post('/update/contact','AdminContactController@update');

  //language   
  Route::get('/language','AdminLanguageController@language');
  Route::get('/edit/language/{id}','AdminLanguageController@edit');        
  Route::post('/create/language','AdminLanguageController@create');        
  Route::post('/update/language','AdminLanguageController@update');        
  Route::delete('/drop/language','AdminLanguageController@drop');        

  //galeery
  Route::get('/galery','AdminGaleryController@galery');
  Route::get('/galery/add','AdminGaleryController@add');
  Route::post('/galery/up','AdminGaleryController@up');

  //message
  Route::get('/message','AdminMessageController@index');
  Route::delete('/delete/message/{id}','AdminMessageController@delete'); 