<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gambar;
use App\News;
use File;
use Cookie;

class AdminNewsController extends Controller
{
	public function news()
	{   $bhs = Cookie::get('bhs_admin');
      $news= News::where('lang','=',$bhs)->get();
	  	return view('admin.page.news',compact('news'));
   	}
	public function editnews($id)
	{
      $new=News::findorFail($id);
	  	return view('admin.page.edit_news',compact('new'));
   	}
	public function addnews()
	{
	  	return view('admin.page.add_news');
   	}
   		public function create(Request $request)
   	{      
        $news = new News();
   	    if( $request->gbr==null){
               $news->gbr='no-thumb.png';
        }else{
                $gambar = $request->file('gbr');
                $namaFile = rand(1111,9999)."_".$gambar->getClientOriginalName();
                $request->file('gbr')->move('img/news', $namaFile);
                $news->gbr=$namaFile;
       }
   		 $news->isi=$request->isi;
   		 $news->judul=$request->judul;
   		 $news->lang=$request->lang;
       $news->save();
      
        return /*dd($request)*/     redirect('/admin/news/');
   	}
   	public function update(Request $request)
   	{   
      $news = News::findorFail($request->id);

        if($files=$request->file('gbr')){
            //HAPUS GAMBAR
             $gbrs = Gambar::where('id_owner', '=',$request->id)->get();
                foreach($gbrs as $file){
                $image_path = public_path().'/img/news/'.$file->gbr;
                $file->delete();
                if(File::exists($image_path)){unlink($image_path);}
              }
              //UPLOAD GAMBAR
          foreach($files as $file){
            $ext=$file->getClientOriginalExtension();
            $name= rand(1111111,9999999).".".$ext;
            $file->move('img/news',$name);
                //save to databse
            $data = new Gambar();
                $data->id_owner = $request->id;
                $data->gbr = $name;
                $data->save();
          }
            $news->gbr=$name;
      }
       $news->isi=$request->isi;
       $news->judul=$request->judul;
       $news->lang= $request->lang;
       $news->save();
      
        return /*dd($gbrs,$image_path) */    redirect('/admin/news/');
   	}
   
   	public function destroy($id)
   	{
   		      $news = News::findorFail($id);
             $gbrs = Gambar::where('id_owner', '=',$id)->get();
                foreach($gbrs as $file){
                $image_path = public_path().'/img/news/'.$file->gbr;
                $file->delete();
                if(File::exists($image_path)){unlink($image_path);}
              }
            $news->delete();
            return /*dd($gbrs,$image_path) */    redirect('/admin/news/');
   	}
}
