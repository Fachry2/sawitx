<?php

namespace App\Http\Controllers;
use App\About;
use File;
use Cookie;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

class AdminAboutController extends Controller
{
	public function about()
	{   
      $bhs = Cookie::get('bhs_admin');
      $abouts = About::where('lang','=',$bhs)->get();
	  	return view('admin.page.about',compact('abouts'));
   	}
	public function edit($id)
	{    
      $about = About::findOrFail($id);
	  	return view('admin.page.edit_about',compact('about'));
   	}

   	 public function update(Request $request)
    {
        $about = About::findOrFail($request->id);
        $about->isi = $request->isi;
        if($about->gbr ==$request->gbr || $request->gbr==null){
               $about->gbr=$about->gbr;
        }else{
              $image_path = public_path().'/img/about/'.$about->gbr;
                if(File::exists($image_path)){unlink($image_path);}
                 $gambar = $request->file('gbr');
                 $namaFile = rand(1111,9999)."_".$gambar->getClientOriginalName();
                 $request->file('gbr')->move('img/about', $namaFile);
                 $about->gbr=$namaFile;
       }
        $about->visi_misi=$request->visi_misi;
        $about->save();
      
        return redirect('/admin/about/');
    }
    public function test(Request $request)
    {
      return response()->json(array("request"=>$request), 200);
    }
   	
}
