<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cookie;
use DB;
use Session; 
use App\Price;
use App\Location;
use App\News;
use App\Gambar;
use App\About;
use App\Contact;
use App\Message;



class HomeController extends Controller
{

  public function index(Request $request)
   { 
    $bhs = Cookie::get('bhs');
    if($bhs ==null ){  
           $bhs='indonesia';
      return redirect('/')->withCookie(cookie()->forever('bhs', $bhs));
        }
    $images=DB::select('select * from laradrop_files where parent_id=9 AND has_thumbnail=1');
    $prices = Price::all();
    $location=Location::findorFail(1);
    $news=News::where('lang','=',$bhs)->get();
  	return  view('page.home',compact('images','prices','location','news'));
   }
    public function contact()
   {    
    $bhs = Cookie::get('bhs');
    $contacts=Contact::where('lang','=',$bhs)->get();
    return view('page.contact',compact('contacts'));
   }
     public function about()
   { 
    $bhs = Cookie::get('bhs');
    $abouts = About::where('lang','=',$bhs)->get();
    return view('page.about',compact('abouts'));
   }
     public function gallery()
   {
    $images=DB::select('select * from laradrop_files');
  	return view('page.gallery',compact('images'));
   }
     public function news()
   {    
    $bhs = Cookie::get('bhs');
    $news=News::where('lang','=',$bhs)->get();
  	return view('page.news',compact('news'));
   }
     public function product()
   {
  	return view('page.product');
   }
     public function read_news($id)
   {
    $images=News::where('id','=',$id)->get();
    $new=News::findorFail($id);
  	return view('page.read_news',compact('images','new'));
   }

   public function images()
   {
    $images=DB::select('select * from laradrop_files');
    return view('page.image',compact('images'));

   }
    public function videos()
   {
    $images=DB::select('select * from laradrop_files');
    return view('page.video',compact('images'));

   }
   public function create_message(Request $request)
   {
    if($request->isi!=null){
    $message= new Message();
    $message->isi=  $request->isi;
    $message->nama=    $request->nama;
    $message->tel=    $request->tel;
    $message->email=    $request->email;
    $message->subject=    $request->subject;
    $message->save();

    Session::flash('flash_message', 'Message Delivered');
    Session::flash('flash_type', 'alert-success');}
    else{
      Session::flash('flash_message', 'please fill all field');
    Session::flash('flash_type', 'alert-danger');}
    
         return redirect('/contact');

   }
   
    public function setCookie($lang){      
       return back()->withCookie(cookie()->forever('bhs', $lang));
   }
}
