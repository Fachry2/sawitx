<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminProductController extends Controller
{
	public function product()
	{
	  	return view('admin.page.product');
   	}
	public function addproduct()
	{
	  	return view('admin.page.add_product');
   	}
	public function editproduct()
	{
	  	return view('admin.page.edit_product');
   	}
}
