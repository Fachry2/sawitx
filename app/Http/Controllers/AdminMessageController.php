<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Message;

class AdminMessageController extends Controller
{
  public function index()
	{  
      $messages = collect(Message::all())->sortByDesc('created_at');
	  	return view('admin.page.message',compact('messages'));
   	}
	public function delete($id)
	{    
      $message = Message::findOrFail($id);
      $message->delete();
	  	return redirect('admin/message');
   	}
}
