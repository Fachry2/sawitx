<?php

namespace App\Http\Controllers;
use Cookie;
use Illuminate\Http\Request;
use App\Contact;
class AdminContactController extends Controller
{
	public function contact()
	{	$bhs = Cookie::get('bhs_admin');
      $contact=Contact::where('lang','=',$bhs)->get();
	  	return view('admin.page.contact',compact('contact'));
   	}
	public function editcontact($id)
	{	$contact=Contact::findorFail($id);
	  	return view('admin.page.edit_contact',compact('contact'));
   	}
   	public function update(Request $request)
   	{
   		$contact=Contact::findorFail($request->id);
   		$contact->isi=$request->isi;
   		$contact->save();

   	return	redirect('/admin/contact');
   	}
}
