<?php

namespace App\Http\Controllers;
use App\Language;
use File;
use Cookie;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

class AdminLanguageController extends Controller
{
	public function language()
	{
		$bahasas = Language::all();
	  	return view('admin.page.language',compact('bahasas'));
   	}
	public function edit($id)
	{
		$bahasa = Language::findOrFail($id);
	  	return view('admin.page.edit_language',compact('bahasa'));
   	}

  public function create(Request $request)
    {  
        $bahasa = new Language();
        $bahasa->bahasa = $request->bahasa;
       if($request->file('gbr')==null){
               return redirect('/admin/gambar_null');
        }else{
                 $gambar = $request->file('gbr');
                 $namaFile ="_lang_".$gambar->getClientOriginalName();
                 $gambar->move('img/bendera', $namaFile);
                 $bahasa->gbr=$namaFile;
       }
        $bahasa->status='unchecked';
        $bahasa->save();
          return redirect('/admin/language');
    }

 	public function update(Request $request)
    {
        $bahasa = Language::findOrFail($request->id);
        $bahasa->bahasa = $request->bahasa;
        if($request->file('gbr')==null){
               $bahasa->gbr=$bahasa->gbr;
        }else{
              $image_path = public_path().'/img/bendera/'.$bahasa->gbr;
                if(File::exists($image_path)){unlink($image_path);}
                 $gambar = $request->file('gbr');
                 $namaFile ="_lang_".$gambar->getClientOriginalName();
                 $gambar->move('img/bendera', $namaFile);
                 $bahasa->gbr=$namaFile;
       }
       if($request->status=="on"){
        $bahasa->status="checked";

       }else{
        $bahasa->status="unchecked";
       }
        $bahasa->save();
        return redirect('/admin/language');
    }
      public function drop($id)
    {   $bahasa = Language::findOrFail($id);
        $bahasa->delete();
        return redirect('/admin/language');
    }
}
