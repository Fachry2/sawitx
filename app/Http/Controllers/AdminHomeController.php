<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Location;
use App\Price;
use Cookie;
use File;
use Auth;

class AdminHomeController extends Controller
{
  public function setCookie($lang){      
       return back()->withCookie(cookie()->forever('bhs_admin', $lang));
   }

  public function index()
  {
    $bhs = Cookie::get('bhs_admin');
        if($bhs ==null ){  
           $bhs='indonesia';
           return redirect('/admin/home/')->withCookie(cookie()->forever('bhs_admin', $bhs));
        }
    $location = Location::findOrFail(1);
    $prices = Price::all();
    return view('admin.page.home',compact('location','prices'));
  }

  public function add_price(Request $request)
  { 
    $price= new Price();
    $price->harga=$request->harga;
    $price->satuan=$request->satuan;
    $price->umur=$request->umur;
    $price->save();

   return  redirect('/admin/home/') ;
  }
   public function up_price(Request $request)
  {
    $price= Price::findOrFail($request->id);
    $price->harga=$request->harga;
    $price->satuan=$request->satuan;
    $price->umur=$request->umur;
    $price->save();

   return  redirect('/admin/home/') ;
  }
  public function delete($id)
  {
       $price= Price::findOrFail($id);
       $price->delete();

        return  redirect('/admin/home/') ;
  }

/*	public function editslide()
	{
	  	return view('admin.page.edit_slide');
   	}*/
	public function editlocation()
	{        
		$location = Location::findOrFail(1);
	  	return view('admin.page.edit_location',compact('location'));
   	}

  public function update(Request $request)
    {
        $location = Location::findOrFail(1);
        if($location->gbr ==$request->gbr || $request->gbr==null){
               $location->gbr=$location->gbr;
        }else{
              $image_path = public_path().'/img/location/'.$location->gbr;
                if(File::exists($image_path)){unlink($image_path);}
                 $gambar = $request->file('gbr');
                 $namaFile = rand(1111,9999)."_".$gambar->getClientOriginalName();
                 $request->file('gbr')->move('img/location', $namaFile);
                 $location->gbr=$namaFile;
       }
        $location->save();
      
        return /*dd($request)*/ redirect('/admin/home/');
    }
}
