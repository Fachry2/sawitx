<?php 
namespace App\Http\Composers;
use Illuminate\Contracts\View\View;
use App\Http\Requests;
use App\Language;
use App\Message;
use Illuminate\Http\Request;
use Cookie;

class Flagcomposer{

	public function compose(View $view)
	{   
		$bahasa=Language::all();
		$bhs = Cookie::get('bhs');
		$bhs2 = Cookie::get('bhs_admin');
		$msg= Message::all();
		if($bhs==null || $bhs2==null ){
    		$view->withCookie(cookie()->forever('bhs', 'indonesia'))->with('flags',$bahasa)->with('bhs',$bhs)->with('bhs_admin',$bhs2)->with('msg',$msg);
		}else{
			$lang=Language::where('bahasa','=',$bhs)->get();
			 $view->with('flags',$bahasa)->with('bhs',$lang[0]->gbr)->with('bhs_admin',$bhs2)->with('msg',$msg);
		}

	}

	public function set_Cookie($bhs){      
       return back()->withCookie(cookie()->forever('bhs', $bhs));
   }










} ?>