<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class flagComposerProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        self::composeFlag();
    }
    public function composeFlag()
    {
    view()->composer('template.header','App\Http\Composers\Flagcomposer@compose');
    view()->composer('admin.page.pil_bhs','App\Http\Composers\Flagcomposer@compose');
    view()->composer('admin.layout.header','App\Http\Composers\Flagcomposer@compose');
    }
}
